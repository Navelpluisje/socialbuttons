<?php

namespace Bolt\Extension\Navelpluisje\Socialbuttons;

use Bolt\Application;
use Bolt\BaseExtension;

class Extension extends BaseExtension
{

    /**
     * Initialize the extension
     */
    public function initialize() {
        $this->addTwigFunction('socialbuttons', 'socialButtons');
    }

    /**
     * @param string $type The type of social buttons we want to show
     * @return \Twig_Markup
     */
    public function socialButtons($type = 'follow') {
        $result = '';
        switch ($type) {
            case 'follow':
                $template = '<a target="_blank" href="%url%" class="%type%-button %account%">%text%</a>';

                foreach ($this->config[$type] as $account => $settings) {
                    if ($settings['name'] != '') {
                        $url     = str_replace( '%name%', $settings['name'], $settings['url'] );
                        $result .= str_replace(
                            ['%url%', '%account%', '%type%', '%text%' ],
                            [$url, $account, $type, $settings['button_text']],
                            $template
                        );
                    }
                }
                break;

            case 'share':
                $template = '<span class="icon social %account%" title="Deel via %account%"><a target="_blank" href="%url%" class="%type%-button %account%">%text%</a></span>';
                $result .= '';

                foreach ($this->config[$type] as $account => $settings) {
                    if (!isset($settings['name']) || $settings['name'] != '') {
                        $url = str_replace(
                            ['%name%', '%text%', '%url%'],
                            [
                                empty( $settings['name'] ) ? '' : $settings['name'],
                                empty( $settings['text'] ) ? '' : $settings['text'],
                                $this->app['paths']['canonicalurl']
                            ],
                            $settings['url']
                        );

                        $result .= str_replace(
                            ['%url%', '%account%', '%type%', '%text%'],
                            [$url, $account, $type, $settings['button_text']],
                            $template
                        );
                    }
                }
                break;
        }

        return new \Twig_Markup($result, 'UTF-8');
    }

    /**
     * Get the name of the extension
     * @return string
     */
    public function getName()
    {
        return "socialbuttons";
    }

}






