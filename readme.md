Social Buttons
==============

The "Social Buttons" is a small extension to insert 'Social' buttons in your templates. Use it by simply placing the following in your template:

    {{ twitterbutton( 'share'/'follow' ) }}

The extension has one parameter 'type'. This can be used to get the 'like'-buttons for posts or 'follow me'-buttons for a footer or homepage.

Config
------
Within the provided there are two examples of both sharing and following. For both the share and follow buttons you can add as many as you want,

share
---
This is how share looks in the `config.yml`

    share:
      twitter:
        url: 'https://twitter.com/intent/tweet?url=%url%&via=%name%&text=%text%'
        name: 'Your-name'
        button_text: 'Share this message on Twitter'
        text: 'This is a real neat article'

You can add for every social medium a new entry. This type of button has a few options:

* **name:** Your name which will be used in the tweet
* **button_text:** Will be the text in the link
* **text:** Can be used with a text placeholder in an url

You can crate an url for all your social medias. Just like in the example above you can use the placeholders. 
The `%url% ` placeholder ins mandatory while this holds the post url to share.

The result will look like:
`<a target="_blank" href="https://twitter.com/intent/tweet?url=http://navelpluisje.nl/entry/neno-the-game&amp;via=Your-name&amp;text=This is a real neat article" class="share-button twitter">Share this message on Twitter</a>`

follow
---
This is how follow looks in the `config.yml`

    follow:
      twitter:
        url: 'https://twitter.com/intent/follow?screen_name=%name%'
        name: 'your-screenname'
        button_text: 'Follow me on Twitter'
        
You can add for every social medium a new entry. This type of button has a few options:

* **url:** The 'follow me' url of the social medium
* **name:** Will be your name or token
* **button_text:** text for the button.

You can crate an url for all your social medias. Just like in the example above you can use the placeholders. 
The `%url% ` placeholder ins mandatory while this holds the post url to share.

The result will look like:
`<a target="_blank" href="https://twitter.com/intent/follow?screen_name=your-screenname" class="follow-button twitter">Follow me on Twitter</a>`

Styling
-------
You can style the buttons the way you want to. Just take a look at the output.


